package vn.laptrinh.football.domain.usecase

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import vn.laptrinh.football.data.source.championship
import vn.laptrinh.football.data.source.CHAMPIONSHIP_NAME
import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.source.local.IChampionshipLocalDataSource
import vn.laptrinh.football.domain.source.remote.IChampionshipRemoteDataSource
import vn.laptrinh.football.domain.source.remote.NetworkResponse
import vn.laptrinh.football.domain.source.remote.RequestStatus.ERR_NETWORK
import vn.laptrinh.football.domain.source.remote.RequestStatus.SUCCESS
import java.net.HttpURLConnection.HTTP_OK

@RunWith(MockitoJUnitRunner::class)
@ExperimentalCoroutinesApi
class ChampionshipUseCaseTest {

    @InjectMocks
    private lateinit var useCase: ChampionshipUseCase

    @Mock
    private lateinit var championshipLDS: IChampionshipLocalDataSource

    @Mock
    private lateinit var championshipRDS: IChampionshipRemoteDataSource

    @Test
    fun `getAndSaveChampionship when Ko`() = runTest {
        // Given
        val networkResponse = NetworkResponse<Championship>(status = ERR_NETWORK)
        given(championshipRDS.getChampionship(CHAMPIONSHIP_NAME)).willReturn(networkResponse)

        // When
        val actual = useCase.getAndSaveChampionship(CHAMPIONSHIP_NAME)

        // Then
        then(championshipRDS).should().getChampionship(CHAMPIONSHIP_NAME)
        assertEquals(networkResponse, actual)
    }

    @Test
    fun `getAndSaveChampionship when Ok`() = runTest {
        // Given
        val networkResponse = NetworkResponse(championship, SUCCESS, HTTP_OK)
        given(championshipRDS.getChampionship(CHAMPIONSHIP_NAME)).willReturn(networkResponse)
        given(championshipLDS.saveChampionship(championship)).willReturn(Unit)

        // When
        val actual = useCase.getAndSaveChampionship(CHAMPIONSHIP_NAME)

        // Then
        then(championshipRDS).should().getChampionship(CHAMPIONSHIP_NAME)
        then(championshipLDS).should().saveChampionship(championship)
        assertEquals(networkResponse, actual)
    }
}