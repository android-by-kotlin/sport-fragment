package vn.laptrinh.football.domain.usecase

import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit4.MockKRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import vn.laptrinh.football.data.source.CHAMPIONSHIP_NAME
import vn.laptrinh.football.data.source.championship
import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.source.local.IChampionshipLocalDataSource
import vn.laptrinh.football.domain.source.remote.IChampionshipRemoteDataSource
import vn.laptrinh.football.domain.source.remote.NetworkResponse
import vn.laptrinh.football.domain.source.remote.RequestStatus.ERR_NETWORK
import vn.laptrinh.football.domain.source.remote.RequestStatus.SUCCESS
import java.net.HttpURLConnection.HTTP_OK

@ExperimentalCoroutinesApi
class ChampionshipUseCaseWithMockKTest {

    @get:Rule
    val mockkRule = MockKRule(this)

    @InjectMockKs
    private lateinit var useCase: ChampionshipUseCase

    @MockK
    private lateinit var championshipLDS: IChampionshipLocalDataSource

    @MockK
    private lateinit var championshipRDS: IChampionshipRemoteDataSource

    @Test
    fun `getAndSaveChampionship when Ko`() = runTest {
        // Given
        val networkResponse = NetworkResponse<Championship>(status = ERR_NETWORK)
        coEvery { championshipRDS.getChampionship(CHAMPIONSHIP_NAME) }.returns(networkResponse)

        // When
        val actual = useCase.getAndSaveChampionship(CHAMPIONSHIP_NAME)

        // Then
        coVerify(exactly = 1) { championshipRDS.getChampionship(CHAMPIONSHIP_NAME) }
        assertEquals(networkResponse, actual)
    }

    @Test
    fun `getAndSaveChampionship when Ok`() = runTest {
        // Given
        val networkResponse = NetworkResponse(championship, SUCCESS, HTTP_OK)
        coEvery { championshipRDS.getChampionship(CHAMPIONSHIP_NAME) }.returns(networkResponse)
        coJustRun { championshipLDS.saveChampionship(championship) }

        // When
        val actual = useCase.getAndSaveChampionship(CHAMPIONSHIP_NAME)

        // Then
        coVerify(exactly = 1) { championshipRDS.getChampionship(CHAMPIONSHIP_NAME) }
        coVerify { championshipLDS.saveChampionship(championship) }
        assertEquals(networkResponse, actual)
    }
}