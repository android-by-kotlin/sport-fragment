package vn.laptrinh.football.data.source.remote

import com.nhaarman.mockitokotlin2.given
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Call
import retrofit2.Response.success
import vn.laptrinh.football.data.source.championship
import vn.laptrinh.football.data.source.CHAMPIONSHIP_NAME
import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.source.remote.NetworkResponse
import vn.laptrinh.football.domain.source.remote.RequestStatus.SUCCESS
import java.net.HttpURLConnection.HTTP_OK

@RunWith(MockitoJUnitRunner::class)
class ChampionshipRemoteDataSourceTest {

    @InjectMocks
    private lateinit var dataSource: ChampionshipRemoteDataSource

    @Mock
    private lateinit var ws: IChampionshipWS

    @Mock
    private lateinit var call: Call<Championship>

    @Test
    fun `getChampionship when call success should return Success NetworkResponse`() = runTest {
        // Given
        given(ws.getRemoteChampionship(CHAMPIONSHIP_NAME)).willReturn(call)
        given(call.execute()).willReturn(success(championship))

        // When
        val result = dataSource.getChampionship(CHAMPIONSHIP_NAME)

        // Then
        assertEquals(result, NetworkResponse(championship, SUCCESS, HTTP_OK))
    }

}