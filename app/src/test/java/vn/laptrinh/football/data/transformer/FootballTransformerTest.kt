package vn.laptrinh.football.data.transformer

import vn.laptrinh.football.data.source.local.db.entity.ChampionshipEntity
import vn.laptrinh.football.domain.model.Championship
import org.junit.Assert.assertEquals
import org.junit.Test

class FootballTransformerTest {

    @Test
    fun toEntity() {
        assertEquals(
            ChampionshipEntity("Ligue 1"),
            Championship("Ligue 1").toEntity()
        )
    }

}