package vn.laptrinh.football.data.source.local.db.dao

import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.apache.commons.lang3.StringUtils.EMPTY
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import vn.laptrinh.football.data.source.local.db.entity.ChampionshipEntity
import vn.laptrinh.football.data.source.local.db.entity.TeamEntity

@ExperimentalCoroutinesApi
@RunWith(RobolectricTestRunner::class)
@HiltAndroidTest
class TeamDaoTest : BaseDaoTest() {

    @Test
    fun findTeamByName() = runTest {
        // Given
        db.championshipDao.insert(ChampionshipEntity(EMPTY))
        val entity = TeamEntity(idTeam = "1", strTeam = "Paris SG")
        db.teamDao.insert(entity)
        db.teamDao.insert(TeamEntity(idTeam = "2", strTeam = "Lyon"))

        // When
        val result = db.teamDao.findTeamByName("Paris SG")

        // Then
        assertEquals(entity, result)
    }

}