package vn.laptrinh.football.data.source.local

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import vn.laptrinh.football.data.source.CHAMPIONSHIP_NAME
import vn.laptrinh.football.data.source.championship
import vn.laptrinh.football.data.source.championshipEntity
import vn.laptrinh.football.data.source.team
import vn.laptrinh.football.data.source.teamEntity
import vn.laptrinh.football.data.source.teamsEntities

@RunWith(MockitoJUnitRunner.Silent::class)
@ExperimentalCoroutinesApi
class ChampionshipLocalDataSourceTest : BaseTestLocalDataSourceWithDb() {

    private lateinit var dataSource: ChampionshipLocalDataSource

    @Before
    fun setUp() {
        dataSource = ChampionshipLocalDataSource(dispatcher, db)
        given(db.championshipDao).willReturn(championshipDao)
        given(db.teamDao).willReturn(teamDao)
    }

    @Test
    fun saveChampionship() = runTest {
        // Given
        given(db.championshipDao.insert(championshipEntity)).willReturn(1)
        given(db.teamDao.insertAll(teamsEntities)).willReturn(LongArray(1))

        // When
        dataSource.saveChampionship(championship)

        // Then
        then(db.championshipDao).should().insert(championshipEntity)
        then(db.teamDao).should().insertAll(teamsEntities)
    }

    @Test
    fun `findTeamByName when not found`() = runTest {
        // Given
        given(db.teamDao.findTeamByName(CHAMPIONSHIP_NAME)).willReturn(null)

        // When
        val result = dataSource.findTeamByName(CHAMPIONSHIP_NAME)

        // Then
        then(db.teamDao).should().findTeamByName(CHAMPIONSHIP_NAME)
        assertNull(result)
    }

    @Test
    fun `findTeamByName when found`() = runTest {
        // Given
        given(db.teamDao.findTeamByName(CHAMPIONSHIP_NAME)).willReturn(teamEntity)

        // When
        val result = dataSource.findTeamByName(CHAMPIONSHIP_NAME)

        // Then
        then(db.teamDao).should().findTeamByName(CHAMPIONSHIP_NAME)
        assertEquals(team, result)
    }

}