package vn.laptrinh.football.data.source.local.db.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room.inMemoryDatabaseBuilder
import dagger.hilt.android.testing.HiltAndroidRule
import vn.laptrinh.football.data.source.local.db.FootballDatabase
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.robolectric.RuntimeEnvironment
import java.io.IOException

open class BaseDaoTest {

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    protected lateinit var db: FootballDatabase

    @Before
    fun createDb() {
        hiltRule.inject()
        db = inMemoryDatabaseBuilder(
            RuntimeEnvironment.getApplication(),
            FootballDatabase::class.java
        ).allowMainThreadQueries().build()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

}
