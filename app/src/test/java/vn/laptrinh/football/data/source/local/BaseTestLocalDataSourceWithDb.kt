package vn.laptrinh.football.data.source.local

import vn.laptrinh.football.data.source.local.db.FootballDatabase
import vn.laptrinh.football.data.source.local.db.dao.ChampionshipDao
import vn.laptrinh.football.data.source.local.db.dao.TeamDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.mockito.Mock

open class BaseTestLocalDataSourceWithDb {

    @Mock
    protected lateinit var db: FootballDatabase

    @Mock
    protected lateinit var championshipDao: ChampionshipDao

    @Mock
    protected lateinit var teamDao: TeamDao

    @ExperimentalCoroutinesApi
    protected val dispatcher = UnconfinedTestDispatcher()

}