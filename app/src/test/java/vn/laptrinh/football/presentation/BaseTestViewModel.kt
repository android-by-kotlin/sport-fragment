package vn.laptrinh.football.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Rule

open class BaseTestViewModel {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

}