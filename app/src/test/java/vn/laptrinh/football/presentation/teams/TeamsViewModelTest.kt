package vn.laptrinh.football.presentation.teams

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.then
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations.openMocks
import org.robolectric.RobolectricTestRunner
import vn.laptrinh.football.LiveDataTestUtil.getValue
import vn.laptrinh.football.data.source.championship
import vn.laptrinh.football.data.source.CHAMPIONSHIP_NAME
import vn.laptrinh.football.data.source.teams
import vn.laptrinh.football.domain.source.remote.NetworkResponse
import vn.laptrinh.football.domain.source.remote.RequestStatus
import vn.laptrinh.football.domain.usecase.ChampionshipUseCase
import vn.laptrinh.football.presentation.BaseTestViewModel
import java.net.HttpURLConnection

@RunWith(RobolectricTestRunner::class)
@ExperimentalCoroutinesApi
class TeamsViewModelTest : BaseTestViewModel() {

    private lateinit var viewModel: TeamsViewModel

    @Mock
    private lateinit var championshipUseCase: ChampionshipUseCase

    @Before
    fun setUp() {
        openMocks(this)
        viewModel = TeamsViewModel(championshipUseCase)
    }

    @Test
    fun `getChampionship when ws Ko`() = runTest {
        // Given
        given(championshipUseCase.getAndSaveChampionship(CHAMPIONSHIP_NAME)).willReturn(NetworkResponse())

        // When
        viewModel.getChampionship(CHAMPIONSHIP_NAME)

        // Then
        then(championshipUseCase).should().getAndSaveChampionship(CHAMPIONSHIP_NAME)
        assertTrue(getValue(viewModel.teams).isEmpty())
    }

    @Test
    fun `getChampionship when ws Ok`() = runTest {
        // Given
        given(championshipUseCase.getAndSaveChampionship(CHAMPIONSHIP_NAME)).willReturn(
            NetworkResponse(
                championship,
                RequestStatus.SUCCESS,
                HttpURLConnection.HTTP_OK
            )
        )

        // When
        viewModel.getChampionship(CHAMPIONSHIP_NAME)

        // Then
        then(championshipUseCase).should().getAndSaveChampionship(CHAMPIONSHIP_NAME)
        assertEquals(teams, getValue(viewModel.teams))
    }
}