package vn.laptrinh.football.data.source

import vn.laptrinh.football.data.transformer.toEntity
import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.model.Team

const val CHAMPIONSHIP_NAME = "Ligue 1"
val team = Team(idTeam = "1", strTeam = "Paris SG")
val teams = listOf(team)
val championship = Championship(
    CHAMPIONSHIP_NAME,
    teams
)
val championshipEntity = championship.toEntity()
val teamEntity = team.toEntity(CHAMPIONSHIP_NAME)
val teamsEntities = listOf(teamEntity)