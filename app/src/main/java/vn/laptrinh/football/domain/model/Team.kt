package vn.laptrinh.football.domain.model

import org.apache.commons.lang3.StringUtils.EMPTY

data class Team(
    val idTeam: String = EMPTY,
    val strTeam: String = EMPTY,
    val strLogo: String? = EMPTY,
    val strBadge: String? = EMPTY,
    val strBanner: String? = EMPTY,
    val strCountry: String? = EMPTY,
    val strLeague: String? = EMPTY,
    val strDescriptionEN: String? = EMPTY
) : BaseModel()
