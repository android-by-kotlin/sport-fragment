package vn.laptrinh.football.domain.usecase

import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.source.local.IChampionshipLocalDataSource
import vn.laptrinh.football.domain.source.remote.IChampionshipRemoteDataSource
import vn.laptrinh.football.domain.source.remote.NetworkResponse
import vn.laptrinh.football.domain.source.remote.RequestStatus
import javax.inject.Inject

class ChampionshipUseCase @Inject constructor(
    private val remoteDataSource: IChampionshipRemoteDataSource,
    private val localDataSource: IChampionshipLocalDataSource
) {

    suspend fun getAndSaveChampionship(
        championshipName: String
    ): NetworkResponse<Championship> {
        val result = remoteDataSource.getChampionship(championshipName)
        if (result.status == RequestStatus.SUCCESS) {
            result.data?.run {
                localDataSource.saveChampionship(
                    this.copy(name = championshipName)
                )
            }
        }
        return result
    }

    suspend fun findTeamByName(
        name: String
    ) = localDataSource.findTeamByName(name)

}