package vn.laptrinh.football.domain.model

data class Championship(
    val name: String,
    val teams: List<Team>? = emptyList()
) : BaseModel()
