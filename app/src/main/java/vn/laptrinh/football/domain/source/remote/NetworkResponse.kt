package vn.laptrinh.football.domain.source.remote

data class NetworkResponse<TModel>(
    val data: TModel? = null,
    val status: RequestStatus = RequestStatus.ERR_NOT_REACHED,
    val code: Int? = null
)

enum class RequestStatus {
    SUCCESS,
    ERR_REJECTED,
    ERR_NOT_REACHED,
    ERR_NETWORK,
    ERR_NO_NETWORK
}
