package vn.laptrinh.football.domain.source.local

import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.model.Team

interface IChampionshipLocalDataSource {

    suspend fun saveChampionship(championship: Championship)

    suspend fun findTeamByName(name: String): Team?

}