package vn.laptrinh.football.domain.source.remote

import vn.laptrinh.football.domain.model.Championship

interface IChampionshipRemoteDataSource {

    suspend fun getChampionship(championshipName: String): NetworkResponse<Championship>

}