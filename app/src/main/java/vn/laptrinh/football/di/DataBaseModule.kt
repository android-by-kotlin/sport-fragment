package vn.laptrinh.football.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.football.data.source.local.db.FootballDatabase
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun providesDatabase(@ApplicationContext context: Context): FootballDatabase {
        synchronized(FootballDatabase::class.java) {
            return Room.databaseBuilder(
                context.applicationContext,
                FootballDatabase::class.java,
                "football.db"
            ).fallbackToDestructiveMigration().build()
        }
    }

}