package vn.laptrinh.football.di

import android.annotation.SuppressLint
import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.football.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Provides
    @SuppressLint("HardwareIds")
    @Singleton
    fun provideOkHttp(
        context: Context
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
        if (vn.laptrinh.football.BuildConfig.DEBUG) {
            builder.addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(vn.laptrinh.football.BuildConfig.BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideGson(): GsonConverterFactory {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setPrettyPrinting()
        gsonBuilder.registerTypeAdapter(
            LocalDateTime::class.java,
            JsonDeserializer { json, _, _ ->
                LocalDateTime.parse(
                    json.asJsonPrimitive.asString,
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME
                )
            }
        )
        gsonBuilder.registerTypeAdapter(
            OffsetDateTime::class.java,
            JsonDeserializer { json, _, _ ->
                OffsetDateTime.parse(
                    json.asJsonPrimitive.asString,
                    DateTimeFormatter.ISO_OFFSET_DATE_TIME
                )
            }
        )

        gsonBuilder.registerTypeAdapter(
            OffsetDateTime::class.java,
            JsonSerializer { date: OffsetDateTime?, _, _ ->
                val dateString = date?.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                JsonPrimitive(dateString)
            }
        )

        return GsonConverterFactory.create(gsonBuilder.create())
    }

}
