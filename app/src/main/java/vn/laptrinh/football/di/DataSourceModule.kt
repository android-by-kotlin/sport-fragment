package vn.laptrinh.football.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.football.data.source.local.ChampionshipLocalDataSource
import vn.laptrinh.football.data.source.remote.ChampionshipRemoteDataSource
import vn.laptrinh.football.domain.source.local.IChampionshipLocalDataSource
import vn.laptrinh.football.domain.source.remote.IChampionshipRemoteDataSource

@InstallIn(SingletonComponent::class)
@Module
abstract class DataSourceModule {

    @Binds
    abstract fun provideChampionshipRemoteDataSource(
        dataSource: ChampionshipRemoteDataSource
    ): IChampionshipRemoteDataSource

    @Binds
    abstract fun provideChampionshipLocalDataSource(
        dataSource: ChampionshipLocalDataSource
    ): IChampionshipLocalDataSource

}
