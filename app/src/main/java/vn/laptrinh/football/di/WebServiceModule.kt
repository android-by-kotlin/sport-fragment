package vn.laptrinh.football.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.football.data.source.remote.IChampionshipWS
import retrofit2.Retrofit

@InstallIn(SingletonComponent::class)
@Module
class WebServiceModule {

    @Provides
    fun provideChampionshipWS(
        retrofit: Retrofit
    ): IChampionshipWS = retrofit.create(IChampionshipWS::class.java)

}
