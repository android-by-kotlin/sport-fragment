package vn.laptrinh.football.data.source.local.db.dao

import androidx.room.Dao
import vn.laptrinh.football.data.source.local.db.entity.ChampionshipEntity
import vn.laptrinh.football.data.source.local.db.relation.ChampionshipWithTeams

@Dao
abstract class ChampionshipDao : BaseDao<ChampionshipEntity, ChampionshipWithTeams>()