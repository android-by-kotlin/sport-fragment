package vn.laptrinh.football.data.source.remote

import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.source.remote.IChampionshipRemoteDataSource
import javax.inject.Inject

class ChampionshipRemoteDataSource @Inject constructor(
    private val ws: IChampionshipWS
) : ApiDataSource<Championship>(), IChampionshipRemoteDataSource {

    override suspend fun getChampionship(
        championshipName: String
    ) = launchRequest(ws.getRemoteChampionship(championshipName))

}

