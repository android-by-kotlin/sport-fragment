package vn.laptrinh.football.data.source.local.db.dao

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.reflect.ParameterizedType

/**
 * @param <E> The Entity
 * @param <DR> The Domain or Relation
 */
@Dao
abstract class BaseDao<E, DR>(private val dispatcher: CoroutineDispatcher = Dispatchers.IO) {

    private val tableName: String
        get() {
            val clazz = (javaClass.superclass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<*>
            return clazz.simpleName
        }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insert(obj: E): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertAll(obj: List<E>): LongArray

    @Delete
    abstract suspend fun delete(obj: E)

    suspend fun deleteAll(): Int {
        return withContext(dispatcher) {
            val query = SimpleSQLiteQuery(
                "DELETE from $tableName"
            )
            doDeleteAll(query)
        }
    }

    suspend fun findAllValid(): List<E> {
        return withContext(dispatcher) {
            val query = SimpleSQLiteQuery(
                "SELECT * FROM $tableName"
            )
            doFindAllValid(query)
        }
    }

    suspend fun findWithRelationClass(id: Long): DR? {
        return withContext(dispatcher) {
            val query = SimpleSQLiteQuery(
                "SELECT * FROM $tableName WHERE id = ?", arrayOf<Any>(id)
            )
            doFindWithRelation(query)
        }
    }

    suspend fun findAllWithRelationClass(): List<DR> {
        return withContext(dispatcher) {
            val query = SimpleSQLiteQuery(
                "SELECT * FROM $tableName"
            )
            doFindAllWithRelation(query)
        }
    }

    suspend fun find(id: Long): E? {
        return withContext(dispatcher) {
            val query = SimpleSQLiteQuery(
                "SELECT * FROM $tableName WHERE id = ?", arrayOf<Any>(id)
            )
            doFind(query)
        }
    }

    suspend fun delete(id: Long) {
        return withContext(dispatcher) {
            val query = SimpleSQLiteQuery(
                "DELETE FROM $tableName WHERE id = ?", arrayOf<Any>(id)
            )
            doDelete(query)
        }
    }

    @RawQuery
    protected abstract suspend fun doFindAllValid(query: SupportSQLiteQuery): List<E>

    @RawQuery
    protected abstract suspend fun doFindWithRelation(query: SupportSQLiteQuery): DR?

    @RawQuery
    protected abstract suspend fun doFindAllWithRelation(query: SupportSQLiteQuery): List<DR>

    @RawQuery
    protected abstract suspend fun doFind(query: SupportSQLiteQuery): E

    @RawQuery
    protected abstract suspend fun doDelete(query: SupportSQLiteQuery): Int

    @RawQuery
    protected abstract suspend fun doDeleteAll(query: SupportSQLiteQuery): Int

    @RawQuery
    protected abstract suspend fun doUpdateAll(query: SupportSQLiteQuery): Int

    @RawQuery
    protected abstract suspend fun doUpdate(query: SupportSQLiteQuery): Int

}