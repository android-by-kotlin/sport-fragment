package vn.laptrinh.football.data.source.local

import vn.laptrinh.football.data.source.local.db.FootballDatabase
import vn.laptrinh.football.data.transformer.toEntity
import vn.laptrinh.football.data.transformer.toModel
import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.source.local.IChampionshipLocalDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ChampionshipLocalDataSource @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val db: FootballDatabase
) : IChampionshipLocalDataSource {

    override suspend fun saveChampionship(championship: Championship) {
        withContext(dispatcher) {
            db.championshipDao.insert(championship.toEntity())
            championship.teams.orEmpty().map {
                it.toEntity(championship.name)
            }.let { teams ->
                db.teamDao.insertAll(teams)
            }
        }
    }

    override suspend fun findTeamByName(name: String): Team? {
        return withContext(dispatcher) {
            db.teamDao.findTeamByName(name)?.toModel()
        }
    }

}