package vn.laptrinh.football.data.source.remote

import vn.laptrinh.football.domain.model.Championship
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IChampionshipWS {

    @GET("search_all_teams.php?")
    fun getRemoteChampionship(
        @Query("l") championshipName: String
    ): Call<Championship>

}