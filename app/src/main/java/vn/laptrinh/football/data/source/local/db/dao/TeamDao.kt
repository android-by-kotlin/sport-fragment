package vn.laptrinh.football.data.source.local.db.dao

import androidx.room.Dao
import androidx.room.Query
import vn.laptrinh.football.data.source.local.db.entity.TeamEntity
import vn.laptrinh.football.domain.model.Team

@Dao
abstract class TeamDao : BaseDao<TeamEntity, Team>() {

    @Query("SELECT * FROM TeamEntity WHERE strTeam = :name LIMIT 1")
    abstract suspend fun findTeamByName(name: String): TeamEntity?

}