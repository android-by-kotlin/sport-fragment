package vn.laptrinh.football.data.source.local.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ChampionshipEntity(
    @PrimaryKey val name: String
)