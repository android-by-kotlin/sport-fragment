package vn.laptrinh.football.data.source.local.db.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.apache.commons.lang3.StringUtils.EMPTY

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = ChampionshipEntity::class,
            parentColumns = ["name"],
            childColumns = ["championshipName"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("championshipName")
    ]
)
data class TeamEntity(
    @PrimaryKey val idTeam: String,
    val championshipName: String = EMPTY,
    val strTeam: String = EMPTY,
    val strLogo: String? = EMPTY,
    val strBadge: String? = EMPTY,
    val strBanner: String? = EMPTY,
    val strCountry: String? = EMPTY,
    val strLeague: String? = EMPTY,
    val strDescriptionEN: String? = EMPTY
)