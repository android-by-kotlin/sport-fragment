package vn.laptrinh.football.data.transformer

import vn.laptrinh.football.data.source.local.db.entity.ChampionshipEntity
import vn.laptrinh.football.data.source.local.db.entity.TeamEntity
import vn.laptrinh.football.domain.model.Championship
import vn.laptrinh.football.domain.model.Team

fun Championship.toEntity() = ChampionshipEntity(
    name = name
)

fun Team.toEntity(championshipName: String) = TeamEntity(
    idTeam = idTeam,
    championshipName = championshipName,
    strTeam = strTeam,
    strLogo = strLogo,
    strBadge = strBadge,
    strBanner = strBanner,
    strCountry = strCountry,
    strLeague = strLeague,
    strDescriptionEN = strDescriptionEN
)

fun TeamEntity.toModel() = Team(
    idTeam = idTeam,
    strTeam = strTeam,
    strLogo = strLogo,
    strBadge = strBadge,
    strBanner = strBanner,
    strCountry = strCountry,
    strLeague = strLeague,
    strDescriptionEN = strDescriptionEN
)