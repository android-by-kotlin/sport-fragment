package vn.laptrinh.football.data.source.local.db.relation

import androidx.room.Embedded
import androidx.room.Relation
import vn.laptrinh.football.data.source.local.db.entity.ChampionshipEntity
import vn.laptrinh.football.data.source.local.db.entity.TeamEntity

data class ChampionshipWithTeams(

    @Embedded
    val championship: ChampionshipEntity,

    @Relation(
        parentColumn = "name",
        entityColumn = "championshipName"
    )
    val teams: List<TeamEntity> = emptyList()

)