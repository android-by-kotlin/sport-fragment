package vn.laptrinh.football.extensions

import vn.laptrinh.football.domain.source.remote.NetworkResponse
import vn.laptrinh.football.domain.source.remote.RequestStatus
import java.io.IOException

fun Exception.toNetworkResponse() = when (this) {
    is IOException -> NetworkResponse<Any>(null, RequestStatus.ERR_NETWORK)
    else -> NetworkResponse<Any>(null, RequestStatus.ERR_NOT_REACHED)
}