package vn.laptrinh.football.presentation

interface IDiffItemCallback<K : Any?> {

    fun areItemsTheSame(oldItem: K, newItem: K): Boolean

    fun areContentsTheSame(oldItem: K, newItem: K): Boolean

}