package vn.laptrinh.football.presentation.teams

import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import vn.laptrinh.football.databinding.TeamItemBinding
import vn.laptrinh.football.domain.model.Team

class TeamViewHolder(
    private val binding: TeamItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(
        lcOwner: LifecycleOwner,
        teamsViewModel: TeamsViewModel,
        team: Team
    ) {
        binding.run {
            lifecycleOwner = lcOwner
            val teamItemVM = TeamItemViewModel()
            teamItemVM.setItem(team)
            itemViewModel = teamItemVM
            fragmentViewModel = teamsViewModel
            executePendingBindings()
        }
    }

}