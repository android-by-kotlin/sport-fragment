package vn.laptrinh.football.presentation

interface ITextSearchFilter<K> {
    fun shouldBeDisplayed(constraint: CharSequence?, obj: K): Boolean
}