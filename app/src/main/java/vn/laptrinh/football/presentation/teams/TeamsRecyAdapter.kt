package vn.laptrinh.football.presentation.teams

import android.view.LayoutInflater.from
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import vn.laptrinh.football.databinding.TeamItemBinding
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.presentation.GenericRecyListAdapter
import vn.laptrinh.football.presentation.IDiffItemCallback

class TeamsRecyAdapter(
    list: MutableList<Team>,
    diffCallback: IDiffItemCallback<Team>,
    private val teamsViewModel: TeamsViewModel,
    private val lcOwner: LifecycleOwner
) : GenericRecyListAdapter<Team>(
    list,
    null,
    diffCallback
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = TeamViewHolder(
        TeamItemBinding.inflate(
            from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(
        viewHolder: RecyclerView.ViewHolder,
        position: Int
    ) {
        (viewHolder as TeamViewHolder).bind(lcOwner, teamsViewModel, currentList[position])
    }

}