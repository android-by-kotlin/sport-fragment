package vn.laptrinh.football.presentation.teams

import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.presentation.IDiffItemCallback

class TeamDiffCallback : IDiffItemCallback<Team> {

    override fun areItemsTheSame(oldItem: Team, newItem: Team) = oldItem.idTeam == newItem.idTeam

    override fun areContentsTheSame(oldItem: Team, newItem: Team) = oldItem == newItem

}