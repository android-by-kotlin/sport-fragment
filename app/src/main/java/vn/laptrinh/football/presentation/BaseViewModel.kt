package vn.laptrinh.football.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
open class BaseViewModel @Inject constructor() : ViewModel() {

    private val _workState: MutableLiveData<WorkState> = MutableLiveData(WorkState.NOT_YET)
    val workState: LiveData<WorkState> get() = _workState

    fun execute(work: suspend () -> Unit) = viewModelScope.launch {
        _workState.value = WorkState.IN_PROGRESS
        work()
        _workState.value = WorkState.DONE
    }

}