package vn.laptrinh.football.presentation.teams

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import vn.laptrinh.football.R
import vn.laptrinh.football.databinding.TeamsFragmentBinding
import vn.laptrinh.football.presentation.BaseFragment
import vn.laptrinh.football.presentation.FragmentBinding

@AndroidEntryPoint
class TeamsFragment : BaseFragment<TeamsViewModel>() {

    private val dataBinding by FragmentBinding<TeamsFragmentBinding>(R.layout.teams_fragment)
    private lateinit var teamsRecyAdapter: TeamsRecyAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = dataBinding.run {
        lifecycleOwner = viewLifecycleOwner
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        fragmentViewModel.run {
            teams.observe(viewLifecycleOwner) {
                teamsRecyAdapter.updateOriginalList(it)
                teamsRecyAdapter.submitList(it)
            }
            navigateToTeam.observe(viewLifecycleOwner) {
                if (it != true) return@observe
                setNavigateToTeam(false)
                val action = TeamsFragmentDirections.actionChampionshipToTeam()
                action.strTeam = clickedItem.value?.strTeam.orEmpty()
                findNavController().navigate(action)
            }
        }
    }

    private fun initRecyclerView() {
        val teamsList = fragmentViewModel.teams.value.orEmpty().toMutableList()

        teamsRecyAdapter = TeamsRecyAdapter(
            teamsList,
            TeamDiffCallback(),
            fragmentViewModel,
            viewLifecycleOwner
        )

        dataBinding.recyAllItems.run {
            layoutManager = context?.resources?.let { GridLayoutManager(activity, resources.getInteger(R.integer.span_count)) }
            itemAnimator = DefaultItemAnimator()
            adapter = teamsRecyAdapter
        }

        dataBinding.filledTextField.editText?.doOnTextChanged { inputText, _, _, _ ->
            Log.d(TeamsFragment::class.simpleName, "inputText : $inputText")
            fragmentViewModel.getChampionship(inputText.toString())
        }
    }

}