package vn.laptrinh.football.presentation.teamdetail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import vn.laptrinh.football.R
import vn.laptrinh.football.databinding.TeamDetailFragmentBinding
import vn.laptrinh.football.presentation.BaseFragment
import vn.laptrinh.football.presentation.FragmentBinding

@AndroidEntryPoint
class TeamDetailFragment : BaseFragment<TeamDetailViewModel>() {

    private val dataBinding by FragmentBinding<TeamDetailFragmentBinding>(R.layout.team_detail_fragment)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = dataBinding.run {
        lifecycleOwner = viewLifecycleOwner
        viewModel = fragmentViewModel
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val name = TeamDetailFragmentArgs.fromBundle(requireArguments()).strTeam
        Log.d(TeamDetailFragment::class.simpleName, "Name is $name")
        fragmentViewModel.run {
            findTeamByName(name)
        }
    }

}