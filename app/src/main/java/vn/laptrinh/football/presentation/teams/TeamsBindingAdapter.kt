package vn.laptrinh.football.presentation.teams

import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import vn.laptrinh.football.R

@BindingAdapter("strTeamBadge")
fun ImageView.strTeamBadge(url: String?) {
    if (url == null) return
    loadImage(url)
}

private fun ImageView.loadImage(imgUrl: String) {
    val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
    val requestOptions = RequestOptions()
    Glide.with(
        context
    ).load(
        imgUri
    ).apply(
        requestOptions.placeholder(
            R.drawable.loading_animation
        ).error(
            R.drawable.ic_broken_image
        )
    ).into(this)
}

@BindingAdapter("strTeamBanner")
fun ImageView.strTeamBanner(url: String?) {
    if (url == null) return
    loadImage(url)
}