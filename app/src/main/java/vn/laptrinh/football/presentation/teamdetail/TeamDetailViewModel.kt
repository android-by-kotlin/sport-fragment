package vn.laptrinh.football.presentation.teamdetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.usecase.ChampionshipUseCase
import vn.laptrinh.football.presentation.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class TeamDetailViewModel @Inject constructor(
    private val championshipUseCase: ChampionshipUseCase
) : BaseViewModel() {

    private val _team: MutableLiveData<Team> = MutableLiveData(null)
    val team: LiveData<Team> = _team

    fun findTeamByName(name : String) = execute {
        _team.value = championshipUseCase.findTeamByName(name)
        Log.d(TeamDetailViewModel::class.simpleName, "Team : ${_team.value}")
    }

}