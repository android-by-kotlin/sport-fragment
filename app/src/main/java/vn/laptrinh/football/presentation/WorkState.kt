package vn.laptrinh.football.presentation

enum class WorkState {

    NOT_YET, // Pas encore commencer
    IN_PROGRESS, // En cours
    DONE,// Déjà fini
    CANCELED, // Opération annulée
    FAILED; // Some error

    fun isInProgress() = this == IN_PROGRESS

    fun isFailedOrDone() = this in setOf(FAILED, DONE)

}