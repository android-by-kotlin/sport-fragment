package vn.laptrinh.football.presentation.teams

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.presentation.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class TeamItemViewModel @Inject constructor() : BaseViewModel() {

    private val _item: MutableLiveData<Team> = MutableLiveData()

    val item: LiveData<Team> = _item

    fun setItem(
        team: Team?
    ) = team.let { _item.value = it }

}