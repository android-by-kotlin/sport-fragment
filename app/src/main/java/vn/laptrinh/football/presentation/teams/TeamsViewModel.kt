package vn.laptrinh.football.presentation.teams

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import vn.laptrinh.football.domain.model.Team
import vn.laptrinh.football.domain.source.remote.RequestStatus
import vn.laptrinh.football.domain.usecase.ChampionshipUseCase
import vn.laptrinh.football.presentation.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class TeamsViewModel @Inject constructor(
    private val championshipUseCase: ChampionshipUseCase
) : BaseViewModel() {

    private val _teams: MutableLiveData<List<Team>> = MutableLiveData(emptyList())
    val teams: LiveData<List<Team>> = _teams

    private val _navigateToTeam = MutableLiveData(false)
    val navigateToTeam: LiveData<Boolean> = _navigateToTeam

    private val _clickedItem: MutableLiveData<Team> = MutableLiveData(null)
    val clickedItem: LiveData<Team> = _clickedItem

    fun getChampionship(championshipName: String) = execute {
        val result = championshipUseCase.getAndSaveChampionship(championshipName)
        Log.d("ChampionshipViewModel", result.toString())
        _teams.value = when (result.status) {
            RequestStatus.SUCCESS -> result.data?.teams.orEmpty().sortedBy { it.strTeam }
            else -> emptyList()
        }
    }

    fun onItemClicked(team: Team) {
        _clickedItem.value = team
        _navigateToTeam.value = true
    }

    fun setNavigateToTeam(shouldNavigate: Boolean) {
        _navigateToTeam.value = shouldNavigate
    }

}