package vn.laptrinh.football.presentation

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import java.lang.reflect.ParameterizedType

open class BaseFragment<VM : BaseViewModel> : Fragment() {

    private val LOG_TAG: String = BaseFragment::class.java.simpleName

    lateinit var fragmentViewModel: VM
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val parameterizedType = javaClass.genericSuperclass as? ParameterizedType

        @Suppress("UNCHECKED_CAST")
        val vmClass = parameterizedType?.actualTypeArguments?.getOrNull(0) as? Class<VM>?

        if (vmClass != null)
            fragmentViewModel = ViewModelProvider(this)[vmClass]
        else
            Log.i(LOG_TAG, "could not find VM class for $this")
    }
}

